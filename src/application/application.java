package application;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("configuration/ConfigurationWebApp");
        //ApplicationContext context = new AnnotationConfigWebApplicationContext();
    }
}
